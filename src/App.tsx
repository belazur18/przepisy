import { ThemeProvider } from "@mui/material";
import AppRoutes from "app/AppRoutes";
import Footer from "app/components/Footer/Footer";
import Topbar from "app/components/Topbar/Topbar";
import { useIsMobileView, useTheme } from "app/hooks";
import Snowflake from "app/components/Background/Snowflake";

const App = () => {
  const theme = useTheme();

  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);

  return (
    <ThemeProvider theme={theme}>
      <div className="app">
        <Topbar />
        <div className="content">
          {!useIsMobileView() && <Snowflake />}
          <main>
            <AppRoutes />
          </main>
          {!useIsMobileView() && <Footer />}
        </div>
      </div>
    </ThemeProvider>
  );
};

export default App;
