import { address } from "app/config";

export const api = {
  urlGetRecipes: `${address}/GetAll`,
  urlGetRecipe: `${address}/Get`,
  urlAddRecipe: `${address}/Add`,
  urlUpdateRecipe: `${address}/Update`,
  urlDeleteRecipe: `${address}/Delete`,
  urlGetAllTags: `${address}/Tags`
};
