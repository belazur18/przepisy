import { recipes } from "features/recipes/sagas/saga";
import { all, spawn } from "redux-saga/effects";

export function* rootSaga() {
  yield all([spawn(recipes)]);
}
