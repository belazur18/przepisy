export enum RequestStatus {
    "Idle",
    "InProgress",
    "Succeeded",
    "Failed",
}