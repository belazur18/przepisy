import { createTheme } from "@mui/material/styles";
import { useMemo, useRef, useEffect, useState } from "react";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import colors from "./colors.module.scss";
import variables from "./variables.module.scss";
import { useWindowSize } from "use-hooks";
import type { AppDispatch, RootState } from "./store";

// Use throughout your app instead of plain `useDispatch` and `useSelector`
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

export const useTheme = () => {
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          primary: {
            main: colors.primaryColor,
          },
        },
        components: {
          MuiIconButton: {
            styleOverrides: {
              root: {
                height: "fit-content",
              },
            },
          },
        },
      }),
    []
  );
  return theme;
};

export const usePrevious = <T>(value: T) => {
  const ref = useRef<T>();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

/**
 * Listens on redux state changes and returns boolean if both values match
 * @param selector Redux state selector
 * @param previousValueMatch value to match in previous state
 * @param currentValueMatch value to match in current state
 * @returns Boolean
 */
export const useStateWatcher = (selector: any, previousValueMatch: any, currentValueMatch: any) => {
  const [match, setMatch] = useState(false);
  const current = useSelector(selector);
  const previous = usePrevious(current);

  useEffect(() => {
    setMatch(current === currentValueMatch && previous === previousValueMatch);
  }, [current, previous, currentValueMatch, previousValueMatch, setMatch]);
  return match;
};

/**
 * Listens on redux state changes and calls action when both values match
 * @param selector Redux state selector
 * @param previousValueMatch value to match in previous state
 * @param currentValueMatch value to match in current state
 * @param action action to run when states match
 */
export const useStateEffect = (selector: any, previousValueMatch: any, currentValueMatch: any, action: Function) => {
  const matched = useStateWatcher(selector, previousValueMatch, currentValueMatch);
  useEffect(() => {
    matched && typeof action === "function" && action();
  }, [matched, action]);
  return null;
};

export const useIsMobileView = () => {
  const size = useWindowSize();
  const isMobile = useMemo(() => size.width <= parseInt(variables.mobileMaxWidth, 10), [size]);
  return isMobile;
};
