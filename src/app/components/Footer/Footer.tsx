import React from "react";

const Footer = () => {
  return (
    <footer>
      <div>
        <b>Przepisy</b> - Izabela Żurczak 2022®
      </div>
    </footer>
  );
};

export default Footer;
