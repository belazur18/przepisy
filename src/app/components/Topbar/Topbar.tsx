import { Add, Favorite, People, Restaurant } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { useIsMobileView } from "app/hooks";
import { NavLink } from "react-router-dom";

const NavTab = (props: any) => {
  const { to, text, Icon } = props;
  return (
    <NavLink className="nav-tab" to={to}>
      <div className="icon">{Icon}</div>
      <div className="text">{text}</div>
    </NavLink>
  );
};

const Topbar = () => {
  return (
    <nav className="topbar">
      {!useIsMobileView() ? (
        <div className="left-container">
          <NavTab text="Przepisy" to="/recipes" Icon={<Restaurant />} />
          <NavTab text="Ulubione" to="/favorites" Icon={<Favorite />} />
          <NavTab text="Nowy przepis" to="/new-recipe" Icon={<Add />} />
        </div>
      ) : (
        <div className="left-container">
          <NavTab to="/recipes" Icon={<Restaurant />} />
          <NavTab to="/favorites" Icon={<Favorite />} />
          <NavTab to="/new-recipe" Icon={<Add />} />
        </div>
      )}
      {!useIsMobileView() && (
        <div className="right-container">
          <IconButton>
            <People />
          </IconButton>
        </div>
      )}
    </nav>
  );
};

export default Topbar;
