import "./Snowflake.scss";
import bread from "./BackgroundAssets/bread1.png";
import burrito from "./BackgroundAssets/burrito1.png";
import burger from "./BackgroundAssets/cheese-burger1.png";
import ham from "./BackgroundAssets/ham1.png";
import icecream from "./BackgroundAssets/ice-cream1.png";
import milk from "./BackgroundAssets/milk1.png";
import cake from "./BackgroundAssets/piece-of-cake1.png";
import watermelon from "./BackgroundAssets/watermelon1.png";

const Snowflake = () => {
  return (
    <div className="snowflakes" aria-hidden="true">
      <img className="snowflake" src={bread} alt="snowflake" />
      <img className="snowflake" src={burrito} alt="snowflake" />
      <img className="snowflake" src={burger} alt="snowflake" />
      <img className="snowflake" src={ham} alt="snowflake" />
      <img className="snowflake" src={icecream} alt="snowflake" />
      <img className="snowflake" src={milk} alt="snowflake" />
      <img className="snowflake" src={cake} alt="snowflake" />
      <img className="snowflake" src={watermelon} alt="snowflake" />
    </div>
  );
};

export default Snowflake;
