import Favourites from "features/recipes/pages/Favourites";
import NewRecipes from "features/recipes/pages/NewRecipes";
import RecipePage from "features/recipes/pages/RecipePage";
import RecipesPage from "features/recipes/pages/RecipesPage";
import { Route, Routes } from "react-router-dom";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/recipes" element={<RecipesPage />} />
      <Route path="/favorites" element={<Favourites />} />
      <Route path="/recipes/:recipeId" element={<RecipePage />} />
      <Route path="/new-recipe" element={<NewRecipes />} />
      <Route path="/" element={<RecipesPage />} />
    </Routes>
  );
};

export default AppRoutes;
