import createSagaMiddleware from "@redux-saga/core";
import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import reducers from "features/recipes/reducers/reducer";
import { rootSaga } from "./saga";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    reducers,
  },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(rootSaga);
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
