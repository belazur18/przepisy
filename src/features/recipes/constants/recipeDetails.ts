export const GET_RECIPE = "GET_RECIPE";
export const GET_RECIPE_STARTED = "GET_RECIPE_STARTED";
export const GET_RECIPE_SUCCEEDED = "GET_RECIPE_SUCCEEDED";
export const GET_RECIPE_FAILED = "GET_RECIPE_FAILED";

export const DELETE_RECIPE = "DELETE_RECIPE";
export const DELETE_RECIPE_STARTED = "DELETE_RECIPE_STARTED";
export const DELETE_RECIPE_SUCCEEDED = "DELETE_RECIPE_SUCCEEDED";
export const DELETE_RECIPE_FAILED = "DELETE_RECIPE_FAILED";
