export interface RecipeToAdd {
  name: string;
  image: string;
  ingredients: object[];
  steps: object[];
  difficulty: number;
  duration: number;
  tags: object[];
}
