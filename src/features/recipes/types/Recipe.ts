import { RecipeDifficulty } from "./RecipeDifficulty";
import { TagType } from "./TagsType";

export interface Recipe {
  id: number;
  name: string;
  image: string;
  ingredients: object[];
  steps: object[];
  tags: TagType[];
  difficulty: RecipeDifficulty;
  duration: number;
  isFavourite: Boolean;
}
