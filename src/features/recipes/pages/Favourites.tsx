import { useIsMobileView } from "app/hooks";
import React from "react";
import FavouritesGrid from "../components/Favourites/FavouritesGrid";

const Favourites = () => {
  return (
    <React.Fragment>
      {useIsMobileView() && <h3>ULUBIONE</h3>}

      <FavouritesGrid />
    </React.Fragment>
  );
};

export default Favourites;
