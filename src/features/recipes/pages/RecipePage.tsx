import { useParams } from "react-router-dom";
import RecipeDetails from "../components/RecipeDetails/RecipeDetails";

const RecipePage = () => {
  const { recipeId } = useParams();

  return (
    <div>
      <RecipeDetails id={Number(recipeId)} />
    </div>
  );
};

export default RecipePage;
