import NewRecipe from "../components/NewRecipe/NewRecipe";

const NewRecipes = () => {
  return (
    <div>
      <NewRecipe />
    </div>
  );
};

export default NewRecipes;
