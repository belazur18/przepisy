import { useIsMobileView } from "app/hooks";
import React, { useState } from "react";
import RecipesGrid from "../components/RecipesGrid/RecipesGrid";
import Tags from "../components/RecipesGrid/Tags/Tags";

const RecipesPage = () => {

  return (
    <React.Fragment>
      <Tags />
      {useIsMobileView() && <h3>PRZEPISY</h3>}
      <RecipesGrid />
    </React.Fragment>
  );
};
export default RecipesPage;
