import {
  ADD_TO_FAVOURITES,
  GET_FAVOURITE_RECIPES,
  GET_FAVOURITE_RECIPES_FAILED,
  GET_FAVOURITE_RECIPES_STARTED,
  GET_FAVOURITE_RECIPES_SUCCEEDED,
  RECIPE_ADDED_TO_FAVOURITES,
  RECIPE_REMOVED_FROM_FAVOURITES,
  REMOVE_FROM_FAVOURITES,
} from "../constants/Favourites";
import { Recipe } from "../types/Recipe";
export type getFavouriteActionType = {
  type: string;
  id: number;
};

export const getFavouriteRecipes = () => ({ type: GET_FAVOURITE_RECIPES });
export const getFavouriteRecipesStarted = () => ({ type: GET_FAVOURITE_RECIPES_STARTED });
export const getFavouriteRecipesSucceeded = (data: Recipe[]) => ({
  type: GET_FAVOURITE_RECIPES_SUCCEEDED,
  data: data,
});
export const getFavouriteRecipesFailed = (error: string) => ({
  type: GET_FAVOURITE_RECIPES_FAILED,
  error,
});

export const addToFavourites = (id: number) => ({ type: ADD_TO_FAVOURITES, id });
export const removeFromFavourites = (id: number) => ({ type: REMOVE_FROM_FAVOURITES, id });

export const recipeAddedToFavourites = (id: number) => ({ type: RECIPE_ADDED_TO_FAVOURITES, id });
export const recipeRemovedFromFavourites = (id: number) => ({ type: RECIPE_REMOVED_FROM_FAVOURITES, id });
