import {   GET_ALL_TAGS, GET_ALL_TAGS_FAILED, GET_ALL_TAGS_STARTED, GET_ALL_TAGS_SUCCEEDED, SET_TAGS_FILTER } from "../constants/tagsFilter";
import { TagType } from "../types/TagsType";

export const setTagsFilter = (data: TagType) => ({
  type: SET_TAGS_FILTER,
  data: data,
});

export const getAllTags = ()=>({type:GET_ALL_TAGS});
export const getAllTagsStarted = ()=>({type:GET_ALL_TAGS_STARTED});
export const getAllTagsSucceeded = (data:TagType[])=>({type:GET_ALL_TAGS_SUCCEEDED,data:data});
 export const getAllTagsFailed = (error:string)=>({type:GET_ALL_TAGS_FAILED,error});

