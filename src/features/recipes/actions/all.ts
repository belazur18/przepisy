import { GET_ALL_RECIPES, GET_ALL_RECIPES_FAILED, GET_ALL_RECIPES_STARTED, GET_ALL_RECIPES_SUCCEEDED } from "../constants/all";
import { Recipe } from "../types/Recipe";
export type getTagsActionType = {
  type: string;
  tags: number[];
};

export const getAllRecipes = (tags:number[]):getTagsActionType => ({ type: GET_ALL_RECIPES, tags });
export const getAllRecipesStarted = () => ({ type: GET_ALL_RECIPES_STARTED });
export const getAllRecipesSucceeded = (data: Recipe[]) => ({
  type: GET_ALL_RECIPES_SUCCEEDED,
  data: data,
});
export const getAllRecipesFailed = (error: string) => ({
  type: GET_ALL_RECIPES_FAILED,
  error,
});
