import { DELETE_RECIPE, DELETE_RECIPE_FAILED, DELETE_RECIPE_STARTED, DELETE_RECIPE_SUCCEEDED } from "../constants/recipeDetails";
import { Recipe } from "../types/Recipe";
export type deleteRecipeActionType = {
  type: string;
  id: number;
};

export const deleteRecipe = (id: number): deleteRecipeActionType => ({
  type: DELETE_RECIPE,
  id,
});
export const deleteRecipeStarted = () => ({ type: DELETE_RECIPE_STARTED });

export const deleteRecipeSucceeded = (data: Recipe) => ({
  type: DELETE_RECIPE_SUCCEEDED,
  data: data,
});

export const deleteRecipeFailed = (error: string) => ({
  type: DELETE_RECIPE_FAILED,
  error,
});
