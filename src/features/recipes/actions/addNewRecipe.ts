import { ADD_NEW_RECIPE, ADD_NEW_RECIPE_STARTED, ADD_NEW_RECIPE_SUCCEEDED } from "../constants/addNewRecipe";
import { Recipe } from "../types/Recipe";
import { RecipeToAdd } from "../types/RecipeToAdd";

export const addNewRecipe = (data: RecipeToAdd) => ({ type: ADD_NEW_RECIPE, data: data });

export const addNewRecipeStarted = () => ({ type: ADD_NEW_RECIPE_STARTED });

export const addNewRecipeSucceeded = (data: Recipe) => ({ type: ADD_NEW_RECIPE_SUCCEEDED, data: data });
