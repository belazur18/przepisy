import {
  GET_RECIPE,
  GET_RECIPE_FAILED,
  GET_RECIPE_STARTED,
  GET_RECIPE_SUCCEEDED,
} from "../constants/recipeDetails";
import { Recipe } from "../types/Recipe";
export type getRecipeActionType = {
  type: string;
  id: number;
};

export const getRecipe = (id: number): getRecipeActionType => ({
  type: GET_RECIPE,
  id,
});
export const getRecipeStarted = () => ({ type: GET_RECIPE_STARTED });
export const getRecipeSucceeded = (data: Recipe) => ({
  type: GET_RECIPE_SUCCEEDED,
  data: data,
});
export const getRecipeFailed = (error: string) => ({
  type: GET_RECIPE_FAILED,
  error,
});
