import { RequestStatus } from "app/types";
import produce from "immer";
import { keyBy } from "lodash";
import {
  GET_FAVOURITE_RECIPES_FAILED,
  GET_FAVOURITE_RECIPES_STARTED,
  GET_FAVOURITE_RECIPES_SUCCEEDED,
  RECIPE_REMOVED_FROM_FAVOURITES,
} from "../constants/Favourites";
import { Recipe } from "../types/Recipe";

interface stateType {
  data: { [id: number]: Recipe };
  status: RequestStatus;
}

const initialState: stateType = {
  data: {},
  status: RequestStatus.Idle,
};

export const reducer = produce((state, action) => {
  switch (action.type) {
    case GET_FAVOURITE_RECIPES_STARTED:
      state.data = {};
      state.status = RequestStatus.InProgress;
      break;

    case GET_FAVOURITE_RECIPES_SUCCEEDED:
      state.data = keyBy(action.data, (recipe: Recipe) => recipe.id); //konwertuje do słownika (dictionary)
      state.status = RequestStatus.Succeeded;
      break;

    case GET_FAVOURITE_RECIPES_FAILED:
      state.data = {};
      state.status = RequestStatus.Failed;
      break;

    case RECIPE_REMOVED_FROM_FAVOURITES:
      delete state.data[action.id];
      break;
    default:
      return state;
  }
}, initialState);

export default reducer;
