import { RequestStatus } from "app/types";
import produce from "immer";
import {  GET_ALL_TAGS, GET_ALL_TAGS_FAILED, GET_ALL_TAGS_STARTED, GET_ALL_TAGS_SUCCEEDED, SET_TAGS_FILTER } from "../constants/tagsFilter";
import { TagType } from "../types/TagsType";

interface stateType {
  all: TagType[] ,
  selected:Number[],
  status: RequestStatus,
}
   
const initialState: stateType = {
  all:[],
  selected:[],
  status: RequestStatus.Idle,
};

export const reducer = produce((state, action) => {
  switch (action.type) {
    case GET_ALL_TAGS:
      state.data=[]
      state.status=RequestStatus.Idle
      break;
      case GET_ALL_TAGS_STARTED:
      state.data=[]
      state.status=RequestStatus.InProgress
      break;
      case GET_ALL_TAGS_SUCCEEDED:
      state.all=action.data
      state.status=RequestStatus.Succeeded
      break;
      case GET_ALL_TAGS_FAILED:
      state.data=[]
      state.status=RequestStatus.Failed
      break;

    case SET_TAGS_FILTER:
      state.selected = action.data;
      break;
      default:
        return state;
      }
}, initialState);

export default reducer;
