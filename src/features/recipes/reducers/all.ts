import { RequestStatus } from "app/types";
import produce from "immer";
import { GET_ALL_RECIPES_FAILED, GET_ALL_RECIPES_STARTED, GET_ALL_RECIPES_SUCCEEDED } from "../constants/all";
import { RECIPE_ADDED_TO_FAVOURITES, RECIPE_REMOVED_FROM_FAVOURITES } from "../constants/Favourites";
import { Recipe } from "../types/Recipe";
import { keyBy } from "lodash";
import { current } from "@reduxjs/toolkit";

interface stateType {
  data: { [id: number]: Recipe };
  status: RequestStatus;
}

const initialState: stateType = {
  data: {},
  status: RequestStatus.Idle,
};

export const reducer = produce((state, action) => {
  switch (action.type) {
    case GET_ALL_RECIPES_STARTED:
      state.data = {};
      state.status = RequestStatus.InProgress;
      break;

    case GET_ALL_RECIPES_SUCCEEDED:
      state.data = keyBy(action.data, (recipe: Recipe) => recipe.id); //konwertuje do słownika (dictionary)
      state.status = RequestStatus.Succeeded;
      break;

    case GET_ALL_RECIPES_FAILED:
      state.data = {};
      state.status = RequestStatus.Failed;
      break;

    case RECIPE_ADDED_TO_FAVOURITES:
      if (current(state).data[action.id]) state.data[action.id].isFavourite = true;
      //przez immera nie da sie odczytać bezpośrednio state. po to current
      break;

    case RECIPE_REMOVED_FROM_FAVOURITES:
      if (current(state).data[action.id]) state.data[action.id].isFavourite = false;
      break;

    default:
      return state;
  }
}, initialState);

export default reducer;
