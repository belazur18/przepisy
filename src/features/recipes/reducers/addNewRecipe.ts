import { RequestStatus } from "app/types";
import produce from "immer";
import { ADD_NEW_RECIPE, ADD_NEW_RECIPE_STARTED, ADD_NEW_RECIPE_SUCCEEDED } from "../constants/addNewRecipe";
import { RecipeToAdd } from "../types/RecipeToAdd";

interface stateType {
  data: RecipeToAdd | null;
  status: RequestStatus;
}

const initialState: stateType = {
  data: null,
  status: RequestStatus.Idle,
};

export const reducer = produce((state, action) => {
  switch (action.type) {
    case ADD_NEW_RECIPE:
      state.data = action.data;
      state.status = RequestStatus.Idle;
      break;

    case ADD_NEW_RECIPE_STARTED:
      state.data = null;
      state.status = RequestStatus.InProgress;
      break;

    case ADD_NEW_RECIPE_SUCCEEDED:
      state.data = action.data;
      state.status = RequestStatus.Succeeded;
      break;

    default:
      return state;
  }
}, initialState);

export default reducer;
