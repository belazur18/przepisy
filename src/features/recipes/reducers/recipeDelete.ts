import { RequestStatus } from "app/types";
import produce from "immer";
import { DELETE_RECIPE_FAILED, DELETE_RECIPE_STARTED, DELETE_RECIPE_SUCCEEDED } from "../constants/recipeDetails";

interface stateType {
  status: RequestStatus;
}

const initialState: stateType = {
  status: RequestStatus.Idle,
};

export const recipeDeleteReducer = produce((state, action) => {
  switch (action.type) {
    case DELETE_RECIPE_STARTED:
      state.status = RequestStatus.InProgress;
      break;

    case DELETE_RECIPE_SUCCEEDED:
      state.status = RequestStatus.Succeeded;
      break;

    case DELETE_RECIPE_FAILED:
      state.status = RequestStatus.Failed;
      break;

    default:
      return state;
  }
}, initialState);

export default recipeDeleteReducer;
