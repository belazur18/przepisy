import { current } from "@reduxjs/toolkit";
import { RequestStatus } from "app/types";
import produce from "immer";
import { RECIPE_ADDED_TO_FAVOURITES, RECIPE_REMOVED_FROM_FAVOURITES } from "../constants/Favourites";
import { GET_RECIPE_FAILED, GET_RECIPE_STARTED, GET_RECIPE_SUCCEEDED } from "../constants/recipeDetails";
import { Recipe } from "../types/Recipe";

interface stateType {
  data: Recipe | null;
  status: RequestStatus;
}

const initialState: stateType = {
  data: null,
  status: RequestStatus.Idle,
};

export const reducer = produce((state, action) => {
  switch (action.type) {
    case GET_RECIPE_STARTED:
      state.data = null;
      state.status = RequestStatus.InProgress;
      break;

    case GET_RECIPE_SUCCEEDED:
      state.data = action.data;
      state.status = RequestStatus.Succeeded;
      break;

    case GET_RECIPE_FAILED:
      state.data = null;
      state.status = RequestStatus.Failed;
      break;

    case RECIPE_ADDED_TO_FAVOURITES:
      if (current(state).data?.id === action.id) state.data.isFavourite = true;
      break;

    case RECIPE_REMOVED_FROM_FAVOURITES:
      if (current(state).data?.id === action.id) state.data.isFavourite = false;
      break;

    default:
      return state;
  }
}, initialState);

export default reducer;
