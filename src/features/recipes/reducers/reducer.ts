import { combineReducers } from "redux";
import all from "./all";
import recipeDetails from "./recipeDetails";
import newRecipe from "./addNewRecipe";
import recipeDeleteReducer from "./recipeDelete";
import favourites from "./favourites";
import addNewRecipe from "./addNewRecipe";
import tagsFilter from "./tagsFilter";

const reducers = combineReducers({
  all,
  recipeDetails,
  newRecipe,
  recipeDeleteReducer,
  favourites,
  addNewRecipe,
  tagsFilter,
});

export default reducers;
