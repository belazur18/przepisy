import { AccessTime } from "@mui/icons-material";
import { useIsMobileView } from "app/hooks";
import { Duration } from "luxon";
import { RecipePreparationTimeProps } from "./types";

const RecipePreparationTime = (props: RecipePreparationTimeProps) => {
  const { value } = props;
  const time = Duration.fromObject({ minutes: value }).toFormat(
    value > 60 ? "h 'h' mm 'min'" : value === 60 ? "h' h'" : "mm' min'"
  );
  return (
    <div className="recipe-preparation-time">
      <AccessTime />
      {!useIsMobileView() ? (
        <div className="time">{value === 125 ? "więcej niż 2 godziny" : value === 120 ? "2 h" : time}</div>
      ) : (
        <div className="time">{value === 125 ? "2 h +" : value === 120 ? "2 h" : time}</div>
      )}
    </div>
  );
};

export default RecipePreparationTime;
