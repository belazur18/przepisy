import pizza from "./3dgifmaker02526.gif";
const ModalLoader = () => {
  return (
    <div className="loader-modal-backdrop">
      <div className="modal">
        <img src={pizza} />
        <h3>Dodawanie...</h3>
      </div>
    </div>
  );
};

export default ModalLoader;
