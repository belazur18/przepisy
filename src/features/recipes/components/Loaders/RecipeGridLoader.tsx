import { Skeleton } from "@mui/material";
import { useIsMobileView } from "app/hooks";

export const LoaderTile = () => (
  <>
    {!useIsMobileView() ? (
      <div className="loader-tile">
        <Skeleton style={{ width: "306px", height: "200px" }} animation="wave" />
        <div className="loader-tile-description">
          <Skeleton style={{ width: "200px", height: "21px" }} animation="wave" />
          <Skeleton className="circle" style={{ width: "40px", height: "40px" }} variant="circular" animation="wave" />
        </div>
        <Skeleton style={{ marginBottom: 2, width: "306px", height: "21px" }} animation="wave" />
      </div>
    ) : (
      <div className="loader-tile">
        <Skeleton style={{ width: "100%", height: "100px" }} animation="wave" />
        <div className="loader-tile-description">
          <Skeleton style={{ width: "65%", height: "26px" }} animation="wave" />
          <Skeleton className="circle" style={{ height: "30px", width: "30px" }} variant="circular" animation="wave" />
        </div>
        <Skeleton style={{ marginTop: 3, marginBottom: 2, width: "100%", height: "36px" }} animation="wave" />
      </div>
    )}
  </>
);
