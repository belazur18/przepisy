import { Skeleton } from "@mui/material";

const RecipeDetailsLoader = () => {
  return (
    <div className="recipe-details-loader-container">
      <Skeleton style={{ marginBottom: 10, width: "950px", height: "400px" }} animation="wave" />

      <Skeleton style={{ marginBottom: 10, width: "950px", height: "150px" }} animation="wave" />
      <div className="recipe-hero-loader">
        <Skeleton style={{ marginBottom: 10, marginRight: 10, width: "250px", height: "550px" }} animation="wave" />
        <Skeleton style={{ marginBottom: 10, width: "690px", height: "550px" }} animation="wave" />
      </div>
    </div>
  );
};
export default RecipeDetailsLoader;
