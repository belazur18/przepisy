import { Delete, Favorite, FavoriteBorder } from "@mui/icons-material";
import { Chip, Divider, IconButton, Tooltip } from "@mui/material";
import RecipeDifficulty from "../RecipeDifficulty/RecipeDifficulty";
import RecipePreparationTime from "../RecipePreparationTime/RecipePreparationTime";
import { RecipeDetailsProps } from "./types";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "app/store";
import { Recipe } from "features/recipes/types/Recipe";
import { useEffect } from "react";
import { getRecipe } from "features/recipes/actions/recipeDetails";
import defaultImage from "../RecipeTile/default.png";
import { useStateEffect } from "app/hooks";
import { RequestStatus } from "app/types";
import { useNavigate } from "react-router";
import { deleteRecipe } from "features/recipes/actions/recipeDelete";
import RecipeDetailsLoader from "../Loaders/RecipeDetailsLoader";
import { addToFavourites, removeFromFavourites } from "features/recipes/actions/Favourites";
import { orange } from "@mui/material/colors";

const RecipeDetails = (props: RecipeDetailsProps) => {
  const { id } = props;
  const dispatch = useDispatch();
  const recipe = useSelector<RootState, Recipe>((state) => state.reducers.recipeDetails.data);

  const navigate = useNavigate();

  const HandleDelete = async (id: number) => {
    dispatch(deleteRecipe(id));
  };
  useStateEffect(
    (state: RootState) => state.reducers.recipeDeleteReducer.status,
    RequestStatus.InProgress,
    RequestStatus.Succeeded,
    () => navigate("/recipes")
  );
  useEffect(() => {
    dispatch(getRecipe(id));
  }, [dispatch, id]);

  return (
    <>
      {recipe ? (
        <div className="recipe-details">
          <div className="image">
            <img src={recipe?.image || defaultImage} alt="przepis-img" />
            {/* {false ? <img src={recipe?.image || defaultImage} alt="przepis-img" /> : imageLoader} */}
          </div>
          <div className="description">
            <div className="title">
              <div>{recipe?.name}</div>
              <div className="social-icons">
                <Tooltip title={recipe.isFavourite ? "Usuń z ulubionych" : "Dodaj do ulubionych"}>
                  <IconButton onClick={() => dispatch(recipe.isFavourite ? removeFromFavourites(id) : addToFavourites(id))}>
                    {recipe.isFavourite ? (
                      <Favorite sx={{ color: orange[500], fontSize: 35 }} />
                    ) : (
                      <FavoriteBorder color="primary" sx={{ fontSize: 35 }} />
                    )}
                  </IconButton>
                </Tooltip>
              </div>
            </div>
            {recipe?.tags.map((tag: any) => (
              <Chip key={tag?.id} label={tag?.name} variant="outlined" />
            ))}
            <div className="time-difficulty-delete">
              <div className="time-difficulty">
                <div className="preparation-time">
                  <RecipePreparationTime value={recipe?.duration} />
                </div>
                <div className="difficulty">
                  <RecipeDifficulty value={recipe?.difficulty} />
                </div>
              </div>

              <Tooltip title="Usuń przepis">
                <IconButton
                  // disabled
                  onClick={() => HandleDelete(id)}
                >
                  <Delete sx={{ marginRight: 0.7 }} />
                </IconButton>
              </Tooltip>
            </div>
          </div>
          <div className="recipe-hero">
            <div className="ingredients">
              Składniki:
              <ul>
                {recipe?.ingredients.map((ingredient: any) => (
                  <li key={ingredient.id}>{ingredient.description}</li>
                ))}
              </ul>
            </div>
            <Divider variant="middle" />

            <div className="steps">
              Kroki:
              <ol>
                {recipe?.steps.map((step: any) => (
                  <li key={step.id}>{step.description}</li>
                ))}
              </ol>
            </div>
          </div>
        </div>
      ) : (
        <RecipeDetailsLoader />
      )}
    </>
  );
};

export default RecipeDetails;
