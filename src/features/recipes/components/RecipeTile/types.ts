import { Recipe } from "features/recipes/types/Recipe";

export interface RecipeTileProps {
  recipe: Recipe;
}
