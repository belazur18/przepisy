import RecipeDifficulty from "../RecipeDifficulty/RecipeDifficulty";
import RecipePreparationTime from "../RecipePreparationTime/RecipePreparationTime";
import { RecipeTileProps } from "./types";
import defaultImage from "./default.png";
import { IconButton, Tooltip } from "@mui/material";
import { Favorite, FavoriteBorder } from "@mui/icons-material";
import { useDispatch } from "react-redux";
import { addToFavourites, removeFromFavourites } from "features/recipes/actions/Favourites";
import { useNavigate } from "react-router";
import { useIsMobileView } from "app/hooks";

const RecipeTile = (props: RecipeTileProps) => {
  const { recipe } = props;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  return (
    <div className="recipe-tile" onClick={() => navigate(`/recipes/${recipe.id}`)}>
      <div className="image">
        <img src={recipe.image || defaultImage} alt="przepis-img" />
      </div>

      <div className="description">
        {useIsMobileView() === false ? (
          <div className="title-duration">
            <div className="title">{recipe.name}</div>
            <div className="preparation-time">
              <RecipePreparationTime value={recipe.duration} />
            </div>
          </div>
        ) : (
          <div className="title">{recipe.name}</div>
        )}
        {useIsMobileView() === false ? (
          <div className="favourites-difficulty">
            <Tooltip title={recipe.isFavourite ? "Usuń z ulubionych" : "Dodaj do ulubionych"}>
              <IconButton
                onClick={(e) => {
                  e.stopPropagation();
                  dispatch(recipe.isFavourite ? removeFromFavourites(recipe.id) : addToFavourites(recipe.id));
                }}
              >
                {recipe.isFavourite ? (
                  <Favorite sx={{ fontSize: 30 }} color="primary" />
                ) : (
                  <FavoriteBorder sx={{ fontSize: 30 }} color="primary" />
                )}
              </IconButton>
            </Tooltip>
            <RecipeDifficulty value={recipe.difficulty} />
          </div>
        ) : (
          <div className="favourites-difficulty-mobile">
            <RecipePreparationTime value={recipe.duration} />
            <RecipeDifficulty value={recipe.difficulty} />
            <Tooltip title={recipe.isFavourite ? "Usuń z ulubionych" : "Dodaj do ulubionych"}>
              <IconButton
                onClick={(e) => {
                  e.stopPropagation();
                  dispatch(recipe.isFavourite ? removeFromFavourites(recipe.id) : addToFavourites(recipe.id));
                }}
              >
                {recipe.isFavourite ? (
                  <Favorite sx={{ fontSize: 30 }} color="primary" />
                ) : (
                  <FavoriteBorder sx={{ fontSize: 30 }} color="primary" />
                )}
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
    </div>
  );
};

export default RecipeTile;
