import { SpeedOutlined } from "@mui/icons-material";
import { useMemo } from "react";
import { RecipeDifficultyProps } from "./types";

const RecipeDifficulty = (props: RecipeDifficultyProps) => {
  const { value } = props;

  const className = useMemo(() => {
    if (value === 1) return "easy";
    if (value === 2) return "medium";
    if (value === 3) return "hard";

    return "";
  }, [value]);

  return (
    <div className="recipe-difficulty">
      <SpeedOutlined className={className} />
    </div>
  );
};

export default RecipeDifficulty;
