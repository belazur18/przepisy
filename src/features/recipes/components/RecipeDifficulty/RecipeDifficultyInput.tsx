import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import { SpeedOutlined } from "@mui/icons-material";
import { green, red, yellow } from "@mui/material/colors";

const RecipeDifficultyInput = (props: any) => {
  const { value, onChange } = props;

  const handleChange = (event: React.MouseEvent<HTMLElement>, newValue: number) => {
    onChange(newValue);
  };

  return (
    <div className="recipe-difficulty-input">
      <ToggleButtonGroup color="secondary" value={value} exclusive onChange={handleChange}>
        <ToggleButton value={1}>
          <SpeedOutlined sx={{ color: green[600] }} />
        </ToggleButton>
        <ToggleButton value={2}>
          <SpeedOutlined sx={{ color: yellow[700] }} />
        </ToggleButton>
        <ToggleButton value={3}>
          <SpeedOutlined sx={{ color: red[600] }} />
        </ToggleButton>
      </ToggleButtonGroup>
    </div>
  );
};

export default RecipeDifficultyInput;
