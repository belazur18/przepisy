import { RecipeDifficulty } from "features/recipes/types/RecipeDifficulty";

export interface RecipeDifficultyProps {
    value: RecipeDifficulty
}