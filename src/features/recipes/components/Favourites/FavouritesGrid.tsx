import { RootState } from "app/store";
import { RequestStatus } from "app/types";
import { Recipe } from "features/recipes/types/Recipe";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import RecipeTile from "../RecipeTile/RecipeTile";
import { getFavouriteRecipes } from "features/recipes/actions/Favourites";
import { LoaderTile } from "../Loaders/RecipeGridLoader";
import { map, size } from "lodash";
import React from "react";

const FavouritesGrid = () => {
  const dispatch = useDispatch();
  const recipes = useSelector<RootState, Recipe[]>((state) => state.reducers.favourites.data);
  const status = useSelector<RootState, RequestStatus>((state) => state.reducers.favourites.status);

  useEffect(() => {
    dispatch(getFavouriteRecipes());
  }, [dispatch]);

  return (
    <React.Fragment>
      <div className="recipes-grid">
        {status === RequestStatus.InProgress && Array.from(Array(10).keys()).map((i) => <LoaderTile key={i} />)}
        {status === RequestStatus.Failed && <div>BŁĄD </div>}
        {status === RequestStatus.Succeeded && map(recipes, (recipe: Recipe) => <RecipeTile key={recipe.id} recipe={recipe} />)}
      </div>
      {size(recipes) === 0 && (
        <div className="no-favoutrites-text">
          <p>Jeszcze nie masz ulubionych przepisów</p>
          <p>Dodaj ten, który Ci się spodoba</p>
        </div>
      )}
    </React.Fragment>
  );
};

export default FavouritesGrid;
