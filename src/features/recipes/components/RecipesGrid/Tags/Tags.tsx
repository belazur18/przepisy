import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import { RootState } from "app/store";
import { setTagsFilter } from "features/recipes/actions/tagsFilter";
import { TagType } from "features/recipes/types/TagsType";
import { map } from "lodash";
import { useDispatch, useSelector } from "react-redux";

const Tags = () => {
  const dispatch = useDispatch();
  const selectedTags = useSelector<RootState,number[]>((state) => state.reducers.tagsFilter.selected);
  const tags = useSelector<RootState,TagType[]>((state) => state.reducers.tagsFilter.all);

  return (
    <div className="tags-container">
      <ToggleButtonGroup value={selectedTags} onChange={(e,selected)=>{dispatch(setTagsFilter(selected))}}>
        {map(tags, (tag: TagType) => (
          <ToggleButton key={tag.id} value={tag.id}>
            {tag.name}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    </div>
  );
};
export default Tags;
