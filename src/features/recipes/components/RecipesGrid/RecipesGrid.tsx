import { RootState } from "app/store";
import { RequestStatus } from "app/types";
import { getAllRecipes } from "features/recipes/actions/all";
import { Recipe } from "features/recipes/types/Recipe";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import RecipeTile from "../RecipeTile/RecipeTile";
import { LoaderTile } from "../Loaders/RecipeGridLoader";
import { map } from "lodash";

const RecipesGrid = () => {
  const dispatch = useDispatch();
  const recipes = useSelector<RootState, Recipe[]>((state) => state.reducers.all.data);
  const tags = useSelector<RootState,number[]>((state) => state.reducers.tagsFilter.selected);

  const status = useSelector<RootState, RequestStatus>((state) => state.reducers.all.status);

  useEffect(() => {
    dispatch(getAllRecipes(tags));
  }, [dispatch,tags]);

  return (
    <div className="recipes-grid">
      {status === RequestStatus.InProgress && Array.from(Array(10).keys()).map((i) => <LoaderTile key={i} />)}
      {status === RequestStatus.Failed && <span> Błąd </span>}
      {status === RequestStatus.Succeeded && map(recipes, (recipe: Recipe) => <RecipeTile key={recipe.id} recipe={recipe} />)}
    </div>
  );
};

export default RecipesGrid;
