import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import React from "react";

const tags = [
  { id: 1, name: "Kuchnia wielkopolska" },
  { id: 2, name: "Kuchnia azjatycka" },
  { id: 3, name: "Kuchnia włoska" },
  { id: 4, name: "Zupy" },
  { id: 5, name: "Desery" },
  { id: 6, name: "Danie główne" },
  { id: 7, name: "Półprodukty" },
  { id: 8, name: "Wege" },
];

const TagsInput = (props: any) => {
  const { value, onChange } = props;

  const handleChange = (event: React.MouseEvent<HTMLElement>, value: object) => {
    onChange(value);
  };
  return (
    <React.Fragment>
      <ToggleButtonGroup value={value} onChange={handleChange}>
        {tags.map((tag) => (
          <ToggleButton key={tag.id} value={tag}>
            {tag.name}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    </React.Fragment>
  );
};
export default TagsInput;
