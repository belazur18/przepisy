import { useEffect, useState } from "react";
import { Button, TextField, Tooltip } from "@mui/material";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";

const StepsInput = (props: any) => {
  const { value, onChange } = props;
  const [description, setDescription] = useState("");

  const handleSteps = () => {
    if (!description?.trim()) {
      return;
    }
    onChange([...value, { id: value.length, description: description }]);
    setDescription("");
    document.getElementById("steps-input-textarea")?.focus();
  };
  const handleDelete = (id: any) => {
    onChange(
      value.filter((step: { id: any }) => {
        return id !== step.id;
      })
    );
  };

  useEffect(() => {
    const element: any = document.getElementById("steps-input-textarea");
    element?.setCustomValidity(value.length === 0 ? "Dodaj choć jeden krok" : "");
  }, [value]);

  return (
    <div>
      <ol>
        {value.map((step: any) => (
          <li key={step.id}>
            <div>
              {step.description}
              <Tooltip title="Usuń">
                <CancelOutlinedIcon className="delete-button" onClick={() => handleDelete(step.id)} />
              </Tooltip>
            </div>
          </li>
        ))}
      </ol>
      <TextField
        inputProps={{
          autoComplete: "off",
        }}
        id="steps-input-textarea"
        value={description}
        placeholder="Np.:Umyte pyry w mundurkach włożyć do zimnej wody"
        onChange={(event) => setDescription(event.target.value)}
        multiline
        onKeyDown={(event) => {
          if (!event.shiftKey && event.key === "Enter") {
            event.preventDefault();
            handleSteps();
            return;
          }
        }}
      />
      <Button variant="outlined" disabled={!description} onClick={() => handleSteps()}>
        dodaj
      </Button>
    </div>
  );
};

export default StepsInput;
