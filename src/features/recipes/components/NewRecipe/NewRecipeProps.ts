export interface NewRecipeProps {
  name: string;
  time: any;
  difficulty: string;
  tags: {
    description: string;
  }[];
  ingredients: {
    id: number;
    name: string;
  }[];
  steps: {
    id: number;
    description: string;
  }[];
}
