import { useState } from "react";
import Slider from "@mui/material/Slider";

function valueLabelFormat(value: number) {
  const units = ["min", "godz"];

  let unitIndex = 0;
  let scaledValue = value;

  if (scaledValue > 60) {
    scaledValue %= 60;
    return `1 godz ${scaledValue} min`;
  } else {
    return `${scaledValue} ${units[unitIndex]}`;
  }
  //   const units = ["min", "godz"];
  // scaledValue > 60 ? `1 godz ${scaledValue % 60} min` : `${scaledValue} ${units[unitIndex]}`;
}

function valueLabelFormat1(value: number) {
  const minutes = value % 60;
  const hours = value / 60;
  let text = "";
  if (hours > 0) text += `${hours} godz. `;
  if (minutes > 0) text += `${minutes} min. `;
  return text;
}

function calculateValue(value: number) {
  return value;
}

export default function NonLinearSlider() {
  const [value, setValue] = useState<number>(10);

  const handleChange = (event: Event, newValue: number | number[]) => {
    if (typeof newValue === "number") {
      setValue(newValue);
    }
  };
  return (
    <>
      {valueLabelFormat1(calculateValue(value))}
      <Slider
        value={value}
        min={5}
        step={5}
        max={90}
        scale={calculateValue}
        valueLabelFormat={valueLabelFormat}
        onChange={handleChange}
        valueLabelDisplay="off"
        aria-labelledby="non-linear-slider"
      />
    </>
  );
}
