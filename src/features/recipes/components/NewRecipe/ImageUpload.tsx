import { TextField } from "@mui/material";
const ImageUpload = () => {
  const handleFileRead = async (event: any) => {
    const selectedFile = event.target.files[0];
    // if (selectedFile === null) {
    //   defaultImage;
    // }
    const base64 = await convertBase64(selectedFile);
  };

  const convertBase64 = (file: any) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };
  return (
    <div>
      <TextField
        id="originalFileName"
        type="file"
        inputProps={{ accept: "image/*, .xlsx, .xls, .csv, .pdf, .pptx, .pptm, .ppt" }}
        name="originalFileName"
        onChange={(e) => handleFileRead(e)}
      />
      <img />
    </div>
  );
};

export default ImageUpload;
