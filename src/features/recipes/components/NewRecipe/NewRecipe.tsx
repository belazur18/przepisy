import { useState } from "react";
import { Button, TextField } from "@mui/material";
import Divider from "@mui/material/Divider";
import UploadButton from "./UploadButton";
import StepsInput from "./StepsInput";
import IngredientsInput from "./IngredientsInput";
import RecipeDifficultyInput from "../RecipeDifficulty/RecipeDifficultyInput";
import SliderInput from "./SliderInput";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "app/store";
import { addNewRecipe } from "features/recipes/actions/addNewRecipe";
import { useStateEffect } from "app/hooks";
import { RequestStatus } from "app/types";
import { useNavigate } from "react-router";
import ModalLoader from "../Loaders/ModalLoader";
import TagsInput from "./TagsInput";

const NewRecipe = () => {
  const [name, setName] = useState("");
  const [duration, setDuration] = useState(10);
  const [difficulty, setDifficulty] = useState(1);
  const [ingredients, setIngredients] = useState([]);
  const [steps, setSteps] = useState([]);
  const [tags, setTags] = useState([]);
  const [photo, setPhoto] = useState("");

  const userData = {
    name: name,
    duration: duration,
    difficulty: difficulty,
    ingredients: ingredients,
    steps: steps,
    tags: tags,
    image: photo,
  };
  const navigate = useNavigate();
  useStateEffect(
    (state: RootState) => state.reducers.addNewRecipe.status,
    RequestStatus.InProgress,
    RequestStatus.Succeeded,
    () => navigate("/recipes")
  );

  const dispatch = useDispatch();
  const status = useSelector<RootState, RequestStatus>((state) => state.reducers.addNewRecipe.status);

  return (
    <div className="new-recipe">
      <p className="new-recipe-title">Dodaj nowy przepis</p>
      {status === RequestStatus.InProgress ? <ModalLoader /> : null}
      <form
        onSubmit={(e) => {
          dispatch(addNewRecipe(userData));
          e.preventDefault();
        }}
      >
        <div className="name">
          <label htmlFor="name">Nazwa:</label>
          <br />
          <TextField
            variant="outlined"
            id="name"
            required
            placeholder="Np.:Pyry z gzikiem"
            value={name}
            onChange={(event: any) => setName(event?.target.value)}
          />
        </div>

        <Divider variant="middle" light />

        <div className="slider">
          <br />
          <SliderInput id="slider" value={duration} onChange={setDuration} />
        </div>

        <Divider variant="middle" light />

        <div className="difficulty">
          <label htmlFor="difficulty"> Poziom trudności:</label>
          <br />
          <br />
          <RecipeDifficultyInput id="difficulty" required value={difficulty} onChange={setDifficulty} />
        </div>

        <Divider variant="middle" light />

        <div className="ingredients">
          <label htmlFor="ingredients"> Składniki:</label>
          <IngredientsInput id="ingredients" value={ingredients} onChange={setIngredients} />
        </div>

        <Divider variant="middle" light />

        <div className="steps">
          <label htmlFor="steps">Kroki:</label>
          <StepsInput id="steps" value={steps} onChange={setSteps} />
        </div>

        <Divider variant="middle" light />

        <div className="select-tag">
          <label htmlFor="select-tag">Tagi:</label>
          <TagsInput value={tags} onChange={setTags} />
        </div>

        <Divider variant="middle" light />
        <br />
        <div className="photo">
          <label htmlFor="photo">Zdjęcie potrawy:</label>
          <br />
          <UploadButton id="photo" value={photo} onChange={setPhoto} />
        </div>

        <Divider variant="middle" light />
        <br />
        <div className="submit-button">
          <Button type="submit" variant="contained">
            Dodaj przepis
          </Button>
        </div>
      </form>
    </div>
  );
};

export default NewRecipe;
