import { IconButton, styled } from "@mui/material";
import { AddAPhoto } from "@mui/icons-material";

const Input = styled("input")({
  display: "none",
});

const UploadButtons = (props: any) => {
  const { value, onChange } = props;

  const onFileChange = async (event: any) => {
    const selectedFile = event.target.files[0];
    if (selectedFile) {
      const base64 = await convertBase64(selectedFile);
      onChange(base64);
    }
  };
  const convertBase64 = (selectedFile: any) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();

      fileReader.readAsDataURL(selectedFile);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <div>
      <div>
        <label>
          <Input accept="image/*" id="icon-button-file" type="file" onChange={onFileChange} />
          <IconButton color="primary" aria-label="upload picture" component="span">
            <AddAPhoto sx={{ fontSize: 40 }} />
          </IconButton>
        </label>
      </div>
      {value && (
        <div>
          <img className="selected-file" alt="uploaded_photo" src={value} />
        </div>
      )}
    </div>
  );
};
export default UploadButtons;
