import Slider from "@mui/material/Slider";

const SliderInput = (props: any) => {
  const { value, onChange } = props;

  function valueLabelFormat(value: any) {
    if (value > 120) {
      return "więcej niż 2 godziny";
    }

    const minutes = value % 60;
    const hours = Math.floor(value / 60);
    let text = "";
    if (hours > 0) {
      text += `${hours} godz. `;
    }
    if (minutes > 0) {
      text += `${minutes} min. `;
    }

    return text;
  }

  return (
    <div>
      <label> Czas przygotowania: {valueLabelFormat(value)}</label>
      <br />
      <br />
      <Slider
        value={value}
        min={5}
        step={5}
        max={125}
        valueLabelFormat={valueLabelFormat}
        onChange={(e: any) => onChange(e.target.value)}
        valueLabelDisplay="off"
        aria-labelledby="non-linear-slider"
      />
    </div>
  );
};

export default SliderInput;
