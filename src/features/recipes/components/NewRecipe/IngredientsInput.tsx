import { useEffect, useState } from "react";
import { Button, TextField } from "@mui/material";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import Tooltip from "@mui/material/Tooltip";

const IngredientsInput = (props: any) => {
  const { value, onChange } = props;
  const [description, setDescription] = useState("");

  const handleIngredients = () => {
    if (!description?.trim()) {
      return;
    }

    onChange([...value, { id: value.length, description }]);
    setDescription("");
    document.getElementById("ingredients-input-textarea")?.focus();
  };

  const handleDelete = (id: any) => {
    onChange(
      value.filter((ingredient: { id: any }) => {
        return id !== ingredient.id;
      })
    );
  };
  useEffect(() => {
    const element: any = document.getElementById("ingredients-input-textarea");
    element?.setCustomValidity(value.length === 0 ? "Dodaj choć jeden składnik" : "");
  }, [value]);
  return (
    <div>
      <ul>
        {value.map((ingredient: any) => (
          <li key={ingredient.id}>
            <div>
              {ingredient.description}
              <Tooltip title="Usuń">
                <CancelOutlinedIcon className="delete-button" onClick={() => handleDelete(ingredient.id)} />
              </Tooltip>
            </div>
          </li>
        ))}
      </ul>
      <TextField
        inputProps={{
          autoComplete: "off",
        }}
        id="ingredients-input-textarea"
        placeholder="Np.:1 kg pyrów"
        value={description}
        onChange={(event: any) => setDescription(event.target.value)}
        onKeyDown={(event) => {
          if (!event.shiftKey && event.key === "Enter") {
            event.preventDefault();
            handleIngredients();
            return;
          }
        }}
      />
      <Button type="submit" variant="outlined" disabled={!description} onClick={() => handleIngredients()}>
        dodaj
      </Button>
    </div>
  );
};

export default IngredientsInput;
