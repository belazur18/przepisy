import { api } from "app/api";
import axios, { AxiosResponse } from "axios";
import { put, takeLatest, takeEvery, call } from "redux-saga/effects";
import {  getAllTagsFailed, getAllTagsStarted, getAllTagsSucceeded } from "../actions/tagsFilter";
import { GET_ALL_TAGS, GET_ALL_TAGS_FAILED } from "../constants/tagsFilter";
import { TagType } from "../types/TagsType";


const getTags = () => {
    return axios.get<TagType[]>(api.urlGetAllTags);
}

function* onGetAllTags() {
    try {
      yield put(getAllTagsStarted());
      const response: AxiosResponse<TagType[]> = yield call(getTags);
  
      if (response.status === 200) {
          yield put(getAllTagsSucceeded(response.data));
          return;
        }
      } catch (error) {
        console.log(error);
        yield put(
          getAllTagsFailed("Wystąpił błąd podczas pobierania tagów")
      );
    }
  }
  
  function* onGetAllTagsFailed(action: any) {
    yield console.log(action.error);
  }
  
  
  export default function* saga() {
    yield takeLatest([GET_ALL_TAGS], onGetAllTags);
    yield takeEvery(GET_ALL_TAGS_FAILED, onGetAllTagsFailed);
  }
  