import axios, { AxiosResponse } from "axios";
import { call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  getAllRecipesFailed,
  getAllRecipesStarted,
  getAllRecipesSucceeded,
  getTagsActionType,
} from "../actions/all";
import { GET_ALL_RECIPES, GET_ALL_RECIPES_FAILED } from "../constants/all";
import { api } from "../../../app/api";
import { Recipe } from "../types/Recipe";
import { getFavouritesFromStorage } from "./favourites";
import { getAllTags } from "../actions/tagsFilter";


var qs = require("qs");
const getRecipes = (tags:number[]) => {
  // const  tagsIdParams = map(tagsArray.flat(),(tag:TagType)=>tag.id)
  let params: any = { params: { tags } };
  params["paramsSerializer"] = (val: any) =>
  qs.stringify(val, { skipNulls: true });
  return axios.get<Recipe[]>(api.urlGetRecipes, params);
};
function* onGetAllRecipes(action:getTagsActionType) {
  try {
    yield put(getAllRecipesStarted());
    const response: AxiosResponse<Recipe[]> = yield call(getRecipes,action.tags);
    yield put(getAllTags())
    
    if (response.status === 200) {
      const favourites = getFavouritesFromStorage();
      response.data.forEach(
        (recipe) => (recipe.isFavourite = favourites.includes(recipe.id))
        );
        yield put(getAllRecipesSucceeded(response.data));
        return;
      }
    } catch (error) {
      console.log(error);
      yield put(
        getAllRecipesFailed("Wystąpił błąd podczas pobierania przepisów")
    );
  }
}

function* onGetAllRecipesFailed(action: any) {
  yield console.log(action.error);
}


export default function* saga() {
  yield takeLatest([GET_ALL_RECIPES], onGetAllRecipes);
  yield takeEvery(GET_ALL_RECIPES_FAILED, onGetAllRecipesFailed);
}
