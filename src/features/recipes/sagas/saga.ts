import { all, spawn } from "redux-saga/effects";
import allRecipes from "./all";
import recipeDelete from "./recipeDelete";
import recipeDetails from "./recipeDetails";
import favourites from "./favourites";
import addNewRecipe from "./addNewRecipe";
import getAllTags  from "./getAllTags"



export function* recipes() {
  yield all([spawn(allRecipes), spawn(recipeDetails), spawn(recipeDelete), spawn(favourites), spawn(addNewRecipe), spawn(getAllTags)]);
}
