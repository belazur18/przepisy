import axios, { AxiosResponse } from "axios";
import { call, put, takeEvery } from "redux-saga/effects";
import { addNewRecipeStarted, addNewRecipeSucceeded } from "../actions/addNewRecipe";
import { ADD_NEW_RECIPE } from "../constants/addNewRecipe";
import { api } from "../../../app/api";
import { Recipe } from "../types/Recipe";
import { RecipeToAdd } from "../types/RecipeToAdd";

const postRecipe = (data: RecipeToAdd) => axios.post<Recipe>(api.urlAddRecipe, data);
function* onAddNewRecipe(action: any) {
  try {
    yield put(addNewRecipeStarted());
    const response: AxiosResponse<Recipe> = yield call(postRecipe, action.data);

    if (response.status === 200) {
      yield put(addNewRecipeSucceeded(response.data));
      return;
    }
  } catch (error) {
    console.log(error);
  }
}
export default function* saga() {
  yield takeEvery(ADD_NEW_RECIPE, onAddNewRecipe);
}
