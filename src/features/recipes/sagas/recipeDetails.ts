import axios, { AxiosResponse } from "axios";
import { call, put, takeEvery } from "redux-saga/effects";
import { getRecipeActionType, getRecipeStarted, getRecipeSucceeded } from "../actions/recipeDetails";
import { api } from "../../../app/api";
import { GET_RECIPE } from "../constants/recipeDetails";
import { Recipe } from "../types/Recipe";
import { getFavouritesFromStorage } from "./favourites";

const getRecipe = (id: number) => axios.get<Recipe>(api.urlGetRecipe, { params: { id } });

function* onGetRecipe(action: getRecipeActionType) {
  try {
    yield put(getRecipeStarted());
    const response: AxiosResponse<Recipe> = yield call(getRecipe, action.id);

    if (response.status === 200) {
      const favourites = getFavouritesFromStorage();
      response.data.isFavourite = favourites.includes(action.id);
      response.data.tags = [
        { id: 1, name: "Kuchnia wielkopolska" },
        { id: 7, name: "Wege" },
      ];
      yield put(getRecipeSucceeded(response.data));
      return;
    }
  } catch (error) {
    console.log(error);
  }
}
export default function* saga() {
  yield takeEvery(GET_RECIPE, onGetRecipe);
}
