import axios, { AxiosResponse } from "axios";
import { call, put, takeEvery } from "redux-saga/effects";
import { deleteRecipeActionType, deleteRecipeStarted, deleteRecipeSucceeded } from "../actions/recipeDelete";
import { api } from "../../../app/api";
import { DELETE_RECIPE } from "../constants/recipeDetails";
import { Recipe } from "../types/Recipe";

const deleteRecipe = (id: number) => axios.delete(api.urlDeleteRecipe, { data: { id } });

function* onDeleteRecipe(action: deleteRecipeActionType) {
  try {
    yield put(deleteRecipeStarted());
    const response: AxiosResponse<Recipe> = yield call(deleteRecipe, action.id);

    if (response.status === 200) {
      yield put(deleteRecipeSucceeded(response.data));
      return;
    }
  } catch (error) {
    console.log(error);
  }
}
export default function* saga() {
  yield takeEvery(DELETE_RECIPE, onDeleteRecipe);
}
