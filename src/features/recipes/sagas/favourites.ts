import axios, { AxiosResponse } from "axios";
import { call, put, takeEvery, takeLatest } from "redux-saga/effects";
import {
  ADD_TO_FAVOURITES,
  GET_FAVOURITE_RECIPES,
  GET_FAVOURITE_RECIPES_FAILED,
  REMOVE_FROM_FAVOURITES,
} from "../constants/Favourites";
import { Recipe } from "../types/Recipe";
import {
  getFavouriteActionType,
  getFavouriteRecipesFailed,
  getFavouriteRecipesStarted,
  getFavouriteRecipesSucceeded,
  recipeAddedToFavourites,
  recipeRemovedFromFavourites,
} from "../actions/Favourites";
import { api } from "../../../app/api";

var qs = require("qs");

const getFavourites = (ids: any) => {
  let params: any = { params: { ids } };
  params["paramsSerializer"] = (val: any) => qs.stringify(val, { skipNulls: true });

  return axios.get<Recipe[]>(api.urlGetRecipes, params);
};
function* onGetFavouriteRecipes(action: getFavouriteActionType) {
  //pobieram dane z localStorage
  const ids = getFavouritesFromStorage();

  try {
    yield put(getFavouriteRecipesStarted());

    //sprawdzam czy są jakieś id w pobranych z localStorage i jak nie ma to nadpisuję pusty array
    if (ids.length === 0) {
      yield put(getFavouriteRecipesSucceeded([]));
      return;
    }
    //otrzymuję odpowiedź z l:26 axios.get (ids jako params)
    const response: AxiosResponse = yield call(getFavourites, ids);

    if (response.status === 200) {
      //pomysł ulubione opiera się na true/false - stąd isFavourite:boolean
      //tutaj przypisuję sztucznie isFavourite bo na serwerze nie ma typu isFavourite. Każde recipe z odpowiedzi dostaje taki typ
      response.data.forEach((recipe: { isFavourite: boolean; id: number }) => (recipe.isFavourite = ids.includes(recipe.id)));
      //ostateczna odpowiedź
      yield put(getFavouriteRecipesSucceeded(response.data));
      return;
    }
  } catch (error) {
    console.log(error);
    yield put(getFavouriteRecipesFailed("Wystąpił błąd podczas pobierania przepisów"));
  }
}

function* onGetFavouriteRecipesFailed(action: any) {
  yield console.log(action.error);
}
//funkcja dodawania id do localStorage (wywoływana z guzika)
function* onAddToFavourites(action: any) {
  const ids = getFavouritesFromStorage();
  ids.push(action.id);
  saveFavouritesInStorage(ids);
  yield put(recipeAddedToFavourites(action.id));
}
//funkcja usuwania id z localStorage (wywoływana z guzika)
function* onRemoveFromFavourites(action: any) {
  const ids = getFavouritesFromStorage();
  //usuwanie metodą filter()
  saveFavouritesInStorage(ids.filter((id) => action.id !== id));

  yield put(recipeRemovedFromFavourites(action.id));
}
//samo dodawanie tablicy numerów do localStorage
export const saveFavouritesInStorage = (items: number[]) => {
  localStorage.setItem("favourite-recipes", items.join(","));
};
//samo pobieranie tablicy numerów z localStorage i mapowanie ich
export const getFavouritesFromStorage = () => {
  return localStorage.getItem("favourite-recipes")?.split(",").map(Number) ?? [];
};

export default function* saga() {
  yield takeLatest([GET_FAVOURITE_RECIPES], onGetFavouriteRecipes);
  yield takeEvery(GET_FAVOURITE_RECIPES_FAILED, onGetFavouriteRecipesFailed);
  yield takeEvery(ADD_TO_FAVOURITES, onAddToFavourites);
  yield takeEvery(REMOVE_FROM_FAVOURITES, onRemoveFromFavourites);
}
